\contentsline {chapter}{\numberline {1}Useful formulas}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Gaussian Integrals}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Gaussian Distribution}{3}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Gaussian Distribution for One Variable}{3}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Gaussian Distribution for More Than One Variable}{4}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Delta Function}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Euler Integral}{5}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Euler Integral of The First Kind: Beta Function}{5}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Euler Integral of The Second Kind: Gamma Function}{6}{subsection.1.4.2}
\contentsline {section}{\numberline {1.5}Baker-Campbell-Hausdorff Formula}{7}{section.1.5}
\contentsline {section}{\numberline {1.6}Feynman Result}{7}{section.1.6}
\contentsline {section}{\numberline {1.7}Kubo Identity}{7}{section.1.7}
\contentsline {section}{\numberline {1.8}Laguerre Polynomials}{8}{section.1.8}
\contentsline {section}{\numberline {1.9}Cramer's Rule}{8}{section.1.9}
\contentsline {section}{\numberline {1.10}Sherman-Morrison Formula}{8}{section.1.10}
\contentsline {section}{\numberline {1.11}Simple Impurity Model at Zero Temperature}{8}{section.1.11}
\contentsline {section}{\numberline {1.12}Green's Function for Simple Cubic Lattice}{9}{section.1.12}
\contentsline {chapter}{\numberline {2}Coherent States}{11}{chapter.2}
\contentsline {section}{\numberline {2.1}Boson Coherent States}{11}{section.2.1}
\contentsline {section}{\numberline {2.2}Grassmann Algebra}{12}{section.2.2}
\contentsline {section}{\numberline {2.3}Fermion Coherent States}{12}{section.2.3}
\contentsline {chapter}{\numberline {3}Linear Response}{15}{chapter.3}
\contentsline {section}{\numberline {3.1}Perturbations Depending on Time}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Fermi Golden Rule}{16}{section.3.2}
\contentsline {section}{\numberline {3.3}The Generalized Susceptibility}{16}{section.3.3}
\contentsline {section}{\numberline {3.4}The Fluctuation Dissipation Theorem}{17}{section.3.4}
\contentsline {section}{\numberline {3.5}Kubo Greenwood Formula}{18}{section.3.5}
\contentsline {section}{\numberline {3.6}Green Kubo Formula}{19}{section.3.6}
\contentsline {chapter}{\numberline {4}Fluctuations}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}Gaussian Distribution}{23}{section.4.1}
\contentsline {section}{\numberline {4.2}Fluctuations of The Fundamental Thermodynamic quantities}{24}{section.4.2}
\contentsline {section}{\numberline {4.3}Correlations of Fluctuations in Time}{25}{section.4.3}
\contentsline {section}{\numberline {4.4}Onsager's Principle}{27}{section.4.4}
\contentsline {section}{\numberline {4.5}Spectral Resolution of Fluctuations}{28}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Fluctuation of a One Dimensional oscillator}{31}{subsection.4.5.1}
\contentsline {section}{\numberline {4.6}Thermoelectric Fluctuations}{31}{section.4.6}
\contentsline {chapter}{\numberline {5}Coherent Potential Approximation}{33}{chapter.5}
\contentsline {section}{\numberline {5.1}The Hamiltonian for Disordered Electronic Systems}{33}{section.5.1}
\contentsline {section}{\numberline {5.2}Average $T$-Matrix Approximation}{35}{section.5.2}
\contentsline {section}{\numberline {5.3}Average $T$-Matrix Approximation for Binary Alloy}{37}{section.5.3}
\contentsline {section}{\numberline {5.4}Coherent Potential Approximation}{40}{section.5.4}
\contentsline {section}{\numberline {5.5}Anderson Localization}{41}{section.5.5}
\contentsline {chapter}{\numberline {6}Small Polaron}{47}{chapter.6}
\contentsline {section}{\numberline {6.1}Holstein Model}{47}{section.6.1}
\contentsline {section}{\numberline {6.2}Weak Coupling Limit}{47}{section.6.2}
\contentsline {section}{\numberline {6.3}Atomic Limit (Zero Temperature)}{48}{section.6.3}
\contentsline {section}{\numberline {6.4}Atomic Limit (Finite Temperature)}{49}{section.6.4}
\contentsline {section}{\numberline {6.5}The Impurity Analogy for A Single Electron}{51}{section.6.5}
\contentsline {subsection}{\numberline {6.5.1}The Zero Temperature Formalism}{52}{subsection.6.5.1}
\contentsline {subsection}{\numberline {6.5.2}The Finite Temperature Formalism}{53}{subsection.6.5.2}
\contentsline {subsection}{\numberline {6.5.3}Dynamical Mean Field}{54}{subsection.6.5.3}
\contentsline {chapter}{\numberline {7}Physical Constants}{57}{chapter.7}
\contentsline {chapter}{\numberline {8}Programming}{59}{chapter.8}
\contentsline {section}{\numberline {8.1}Linux}{59}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Install fcitx method}{59}{subsection.8.1.1}
\contentsline {section}{\numberline {8.2}\LaTeX }{59}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Beamer}{59}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Angstrom \r A}{59}{subsection.8.2.2}
\contentsline {section}{\numberline {8.3}Emacs}{60}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Miscellaneous}{60}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Problem fixing}{60}{subsection.8.3.2}
\contentsline {subsection}{\numberline {8.3.3}MELPA}{60}{subsection.8.3.3}
\contentsline {subsection}{\numberline {8.3.4}AUCTeX}{61}{subsection.8.3.4}
\contentsline {subsection}{\numberline {8.3.5}Slime}{62}{subsection.8.3.5}
\contentsline {subsection}{\numberline {8.3.6}My .emacs}{62}{subsection.8.3.6}
\contentsline {section}{\numberline {8.4}Perl regular expression}{63}{section.8.4}
\contentsline {subsection}{\numberline {8.4.1}Perl regular expression variable}{63}{subsection.8.4.1}
\contentsline {subsection}{\numberline {8.4.2}Matching floating point numbers}{63}{subsection.8.4.2}
